package hresto.log;

public enum Severity {
    MINOR,
    MAJOR,
    SEVERE,
    FATAL
}
