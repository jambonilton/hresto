package hresto.log;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class LoggingHttpHandler implements HttpHandler {

    final Logger logger;
    final HttpHandler base;

    public LoggingHttpHandler(Logger logger, HttpHandler base) {
        this.logger = logger;
        this.base = base;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        logger.log(exchange.getRequestURL());
        base.handleRequest(exchange);
    }

}
