package hresto.log;

import java.time.Clock;

public interface Logger {

    static Logger console() {
        return new PrintStreamLogger(Clock.systemDefaultZone(), System.out, System.err);
    }

    default void log(Object object) {
        log(String.valueOf(object));
    }

    void log(String message);

    default void log(String messageFormat, Object... args) {
        log(String.format(messageFormat, args));
    }

    void error(Severity severity, Throwable throwable, String message);

    default void error(Severity severity, Throwable throwable, String message, Object... args) {
        error(severity, throwable, String.format(message, args));
    }

}