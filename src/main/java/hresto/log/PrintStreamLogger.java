package hresto.log;

import hresto.sys.util.Exceptions;

import java.io.PrintStream;
import java.time.Clock;

public class PrintStreamLogger implements Logger {

    final Clock clock;
    final PrintStream out, err;

    public PrintStreamLogger() {
        this(Clock.systemUTC(), System.out, System.err);
    }

    public PrintStreamLogger(Clock clock, PrintStream out, PrintStream err) {
        this.clock = clock;
        this.out = out;
        this.err = err;
    }

    @Override
    public void log(String message) {
        out.println(clock.instant() + " " + message);
    }

    @Override
    public void error(Severity severity, Throwable throwable, String message) {
        err.println(clock.instant() + " " + severity+" "+message+"\n\t"+throwable.getClass().getSimpleName()+": "+throwable.getMessage());
        logStackTrace(throwable);
        Throwable cause = throwable.getCause();
        while (cause != null) {
            err.println("\tCaused by: "+cause.getClass().getSimpleName()+": "+cause.getMessage());
            logStackTrace(cause);
            cause = cause.getCause();
        }
    }

    private void logStackTrace(Throwable throwable) {
        err.print(Exceptions.formatStacktrace(throwable));
    }

}
