package hresto.auth;

import java.security.SecureRandom;
import java.util.Random;
import java.util.function.Supplier;

public class StaticLengthRandomStringGenerator implements Supplier<String> {

    private static final char[] DEFAULT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    final int length;
    final Random random;
    final char[] chars;

    public StaticLengthRandomStringGenerator(int length) {
        this(length, new SecureRandom(), DEFAULT_CHARS);
    }

    public StaticLengthRandomStringGenerator(int length, Random random, char[] chars) {
        this.length = length;
        this.random = random;
        this.chars = chars;
    }

    @Override
    public String get() {
        final char[] str = new char[length];
        for (int i=0; i < length; i++)
            str[i] = chars[random.nextInt(chars.length)];
        return new String(str);
    }

}
