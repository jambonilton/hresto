package hresto.auth;

import java.util.Optional;

public interface SessionStore {

    Session create(User user);

    Optional<Session> get(String id);

}
