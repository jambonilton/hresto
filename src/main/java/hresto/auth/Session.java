package hresto.auth;

public class Session {

    final String id;
    final User user;

    public Session(String id, User user) {
        this.id = id;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

}
