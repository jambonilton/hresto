package hresto.auth;

import hresto.rest.BasicResource;
import hresto.sql.annotations.Table;

import java.util.UUID;

@Table("AppUser")
public class User extends BasicResource {

    public static final User GOD_MODE = new User(
            UUID.randomUUID(),
            "n/a",
            "n/a",
            "admin",
            null,
            null,
            null,
            null);

    protected final String externalId;
    protected final String email;
    protected final String name;
    protected final String photoUrl;
    protected final String locale;
    protected final String familyName;
    protected final String givenName;

    public User(UUID id, String externalId, String email, String name, String photoUrl, String locale, String familyName, String givenName) {
        super(id);
        this.externalId = externalId;
        this.email = email;
        this.name = name;
        this.photoUrl = photoUrl;
        this.locale = locale;
        this.familyName = familyName;
        this.givenName = givenName;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getLocale() {
        return locale;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getGivenName() {
        return givenName;
    }

}
