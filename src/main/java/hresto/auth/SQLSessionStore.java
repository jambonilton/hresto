package hresto.auth;

import hresto.sql.exec.SQLExecutor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;

import static hresto.sql.gen.SQLInsertBuilder.insertInto;
import static hresto.sql.gen.SQLQueryBuilder.eq;
import static hresto.sql.gen.SQLQueryBuilder.select;

public class SQLSessionStore implements SessionStore {

    public static final int DEFAULT_LENGTH = 42;

    final Supplier<String> sessionIds;
    final SQLExecutor exec;

    public SQLSessionStore(SQLExecutor exec) {
        this(new StaticLengthRandomStringGenerator(DEFAULT_LENGTH), exec);
    }

    public SQLSessionStore(Supplier<String> sessionIds, SQLExecutor exec) {
        this.sessionIds = sessionIds;
        this.exec = exec;
    }

    @Override
    public Session create(User user) {
        final String sessionId = sessionIds.get();
        exec.apply(insertInto("Session", "id", "sessionUser").values(sessionId, user.getId()));
        return new Session(sessionId, user);
    }

    @Override
    public Optional<Session> get(String id) {
        return exec.query(
                select()
                    .from("Session").join("AppUser", "sessionUser", "id")
                    .where(eq("id", id)), this::getSessionFromResult).findFirst();
    }

    private Session getSessionFromResult(ResultSet rs) throws SQLException {
        final User user = new User(
                UUID.fromString(rs.getString("sessionUser")),
                rs.getString("externalId"),
                rs.getString("email"),
                rs.getString("name"),
                rs.getString("photoUrl"),
                rs.getString("locale"),
                rs.getString("familyName"),
                rs.getString("givenName"));
        return new Session(rs.getString("id"), user);
    }

}
