package hresto.http.handlers;

import jtree.query.Query;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static jtree.query.Query.*;
import static jtree.util.reflect.Conversion.convertTo;

public class UrlFormReflection {

    // TODO
    public static Query getQueryObject(Map<String, Deque<String>> params) {
        if (params.isEmpty())
            return alwaysTrue();
        return Query.and(params.entrySet().stream()
                .map(e -> e.getValue().size() == 1 ? is(e.getKey(), e.getValue().element()) : in(e.getKey(), e.getValue()))
                .collect(toList()));
    }

    private static Optional<String> getParam(Map<String, Deque<String>> params, String field) {
        final Deque<String> values = params.get(field);
        return values == null
                ? Optional.empty()
                : values.stream().findFirst();
    }

    private static <Q> void setValue(Q query, Field field, String value) {
        try {
            field.setAccessible(true);
            field.set(query, convertTo(value, field.getType()));
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getQueryString(Object object) {
        if (object instanceof Map) {
            final Map<Object, Object> map = (Map) object;
            return map.entrySet().stream()
                    .map(e -> toQuery(e.getKey(), e.getValue()))
                    .collect(joining("&"));
        } else {
            return Stream.of(object.getClass().getDeclaredFields())
                    .map(f -> toQuery(f, object))
                    .filter(e -> e != null)
                    .collect(joining("&"));
        }
    }

    private static String toQuery(Field field, Object obj) {
        try {
            field.setAccessible(true);
            final Object value = field.get(obj);
            if (value == null)
                return null;
            return toQuery(field.getName(), value);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private static String toQuery(Object key, Object value) {
        try {
            return key + "=" + URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
