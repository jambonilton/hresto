package hresto.http.handlers;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

import java.util.function.Supplier;

public class RefreshingHandler implements HttpHandler {

    final Supplier<HttpHandler> handlerSupplier;
    HttpHandler handlerInstance;

    public RefreshingHandler(Supplier<HttpHandler> handlerSupplier) {
        this.handlerSupplier = handlerSupplier;
        this.handlerInstance = handlerSupplier.get();
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        handlerInstance.handleRequest(exchange);
    }

    public void refresh() {
        this.handlerInstance = handlerSupplier.get();
    }

}
