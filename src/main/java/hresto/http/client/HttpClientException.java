package hresto.http.client;

public class HttpClientException extends RuntimeException {
    public HttpClientException(Throwable e) {
        super(e);
    }
}
