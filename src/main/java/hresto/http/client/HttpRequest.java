package hresto.http.client;


import jtree.util.io.json.Json;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;

import static hresto.http.handlers.UrlFormReflection.getQueryString;

public class HttpRequest {

    public static final Json JSON = new Json();

    private final URI uri;
    private String method = "GET";
    private ContentType contentType;
    private Object body;
    private String serializedBody;

    public HttpRequest(String uri) {
        try {
            this.uri = new URI(uri);
        } catch (URISyntaxException e) {
            throw new HttpClientException(e);
        }
    }

    public HttpRequest(URI uri) {
        this.uri = uri;
    }

    public URI getURI() {
        return uri;
    }

    public String getMethod() {
        return method;
    }

    public HttpRequest setMethod(String method) {
        this.method = method;
        return this;
    }

    public ContentType getContentType() {
        return contentType == null ? ContentType.FORM : contentType;
    }

    public HttpRequest setContentType(ContentType contentType) {
        this.contentType = contentType;
        return this;
    }

    public HttpRequest setBody(Object body) {
        this.body = body;
        return this;
    }

    public Integer getContentLength() {
        try {
            if (body == null)
                return 0;
            else if (body instanceof String)
                return ((String) body).getBytes().length;
            else if (body instanceof InputStream)
                return ((InputStream) body).available();
            else if (body instanceof ByteBuffer)
                return ((ByteBuffer) body).remaining();
            else
                return getSerializedBody().length();
        } catch (IOException e) {
            throw new HttpClientException(e);
        }
    }

    public InputStream getBody() {
        if (body == null)
            return new ByteArrayInputStream(new byte[0]);
        else if (body instanceof String)
            return new ByteArrayInputStream(((String) body).getBytes());
        else if (body instanceof InputStream)
            return (InputStream) body;
        else if (body instanceof ByteBuffer)
            return new ByteArrayInputStream(((ByteBuffer) body).array());
        else
            return new ByteArrayInputStream(getSerializedBody().getBytes());
    }

    private String getSerializedBody() {
        if (body == null)
            return null;
        else if (serializedBody == null)
            return serializedBody = makeSerializedBody();
        return serializedBody;
    }

    private String makeSerializedBody() {
        if (getContentType().equals(ContentType.FORM))
            return getQueryString(body);
        else if (getContentType().equals(ContentType.JSON))
            return JSON.write(body);
        throw new UnsupportedOperationException("Unknown Content-Type "+contentType);
    }

}
