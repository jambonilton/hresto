package hresto.http.client;

public enum ContentType {

    FORM("application/x-www-form-url-encoded"),
    JSON("application/json");

    private String stringValue;

    ContentType(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }

}
