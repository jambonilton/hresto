package hresto.http.client;

public interface HttpClient {

    HttpResponse send(HttpRequest request);

}
