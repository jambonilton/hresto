package hresto.http.client;

import hresto.sys.io.ByteStreams;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class BasicHttpClient implements HttpClient {

    @Override
    public HttpResponse send(HttpRequest request) {
        try {
            final URL url = request.getURI().toURL();
            final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(request.getMethod());
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("User-Agent", "CHATMASTER9000/0.9");
            if (request.getMethod().equalsIgnoreCase("POST")) {
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty( "Content-Length", String.valueOf(request.getContentLength()));
                ByteStreams.write(request.getBody(), conn.getOutputStream());
            }
            conn.connect();

            return new HttpResponse(conn);
        }
        catch (IOException e) {
            throw new HttpClientException(e);
        }
    }

}
