package hresto.http.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URLConnection;

public class HttpResponse {

    final URLConnection connection;

    public HttpResponse(URLConnection connection) {
        this.connection = connection;
    }

    public InputStream getInputStream() {
        try {
            return connection.getInputStream();
        } catch (IOException e) {
            throw new HttpClientException(e);
        }
    }

    public Reader getReader() {
        try {
            return new InputStreamReader(connection.getInputStream());
        } catch (IOException e) {
            throw new HttpClientException(e);
        }
    }

}
