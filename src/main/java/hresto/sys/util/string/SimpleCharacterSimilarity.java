package hresto.sys.util.string;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class SimpleCharacterSimilarity implements StringSimilarity {

    final int cardinalityLenience;

    public SimpleCharacterSimilarity(int cardinalityLenience) {
        this.cardinalityLenience = cardinalityLenience;
    }

    @Override
    public double test(String s1, String s2) {
        final char[] c1 = s1.toCharArray(),
                     c2 = s2.toCharArray();
        return (test(c1, c2) + test(c2, c1)) / 2d;
    }

    private double test(char[] c1, char[] c2) {
        double matchCount = 0;
        for (int i=0; i < c1.length; i++) {
            for (int j = max(0,i-cardinalityLenience); j < min(i+cardinalityLenience+1, c2.length); j++) {
                if (c1[i] == c2[j]) {
                    matchCount++;
                    break;
                }
            }
        }
        return matchCount / (double) c1.length;
    }

}
