package hresto.sys.util.string;

public interface StringSimilarity {

    /**
     * @return 0 to 1 rating on how similar the two strings are.
     */
    double test(String s1, String s2);

}
