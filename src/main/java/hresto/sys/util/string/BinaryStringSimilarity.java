package hresto.sys.util.string;

public class BinaryStringSimilarity implements StringSimilarity {

    @Override
    public double test(String s1, String s2) {
        return s1.equals(s2) ? 1d : 0d;
    }
}
