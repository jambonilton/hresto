package hresto.sys.util.string;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class WordMatchCountSimilarity implements StringSimilarity {

    final int cardinalityLenience;
    final StringSimilarity wordSimilarity;

    public WordMatchCountSimilarity(int cardinalityLenience, StringSimilarity wordSimilarity) {
        this.cardinalityLenience = cardinalityLenience;
        this.wordSimilarity = wordSimilarity;
    }

    @Override
    public double test(String s1, String s2) {
        final String[] words1 = s1.toLowerCase().split("\\W+"),
                       words2 = s2.toLowerCase().split("\\W+");
        return (test(words1, words2) + test(words2, words1)) / 2d;
    }


    private double test(String[] a, String[] b) {
        double matchCount = 0;
        for (int i=0; i < a.length; i++) {
            double containsWord = 0;
            for (int j=max(0,i-cardinalityLenience); j < min(i+cardinalityLenience+1, b.length); j++)
                containsWord = max(containsWord, wordSimilarity.test(a[i], b[j]));
            matchCount += containsWord;
        }
        return matchCount / (double) a.length;
    }

}
