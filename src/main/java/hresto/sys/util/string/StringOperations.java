package hresto.sys.util.string;

import java.util.function.IntPredicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class StringOperations {

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static String toCamelCase(String s) {
        if (isEmpty(s))
            return "";
        return Stream.of(s.split("\\b"))
                .map(word -> Character.toUpperCase(word.charAt(0)) + word.substring(1))
                .collect(joining());
    }

    public static String removePrefix(String s, String prefix) {
        if (!s.startsWith(prefix))
            throw new IllegalArgumentException("Expected \""+s+"\" to start with \""+prefix+"\".");
        return s.substring(prefix.length());
    }

    public static String trim(String s, IntPredicate characterClass) {
        int i, j;
        for (i=0; i < s.length(); i++)
            if (!characterClass.test(s.charAt(i)))
                break;
        if (i == s.length())
            return "";
        for (j=s.length(); j > 0; j--)
            if (!characterClass.test(s.charAt(j-1)))
                break;
        return s.substring(i, j);
    }

    public static String join(char delim, String... elements) {
        return Stream.of(elements).map(e -> trim(e, c -> c == delim)).collect(joining(String.valueOf(delim)));
    }

}
