package hresto.sys.util;

import java.util.Comparator;
import java.util.function.Function;

public class Comparators {

    public static <E,K extends Comparable<K>> Comparator<E> mapping(Function<E, K> mapper) {
        return (e1, e2) -> mapper.apply(e1).compareTo(mapper.apply(e2));
    }

}

