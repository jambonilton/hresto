package hresto.sys.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Arrays {

    public static <I,O> O[] map(Function<I, O> map, IntFunction<O[]> outputSupplier, I[] input) {
        return Stream.of(input).map(map).toArray(outputSupplier);
    }

    public static <E> Optional<E> find(E[] arr, Predicate<? super E> predicate) {
        return Stream.of(arr).filter(predicate).findAny();
    }

    public static <E> boolean anyMatch(E[] arr, Predicate<? super E> predicate) {
        return Stream.of(arr).anyMatch(predicate);
    }

}
