package hresto.sys.util;

import java.util.Optional;

@FunctionalInterface
public interface Lookup<K,V> {
    Optional<V> find(K key);
}
