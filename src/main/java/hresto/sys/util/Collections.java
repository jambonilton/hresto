package hresto.sys.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Collection-related utility functions.
 */
public interface Collections {

    static <K,V> Map<K,V> toMap(K key, V value, Object... more) {
        final Map<K,V> map = new HashMap<>();
        map.put(key, value);
        if (more.length % 2 != 0)
            throw new IllegalArgumentException("Expected even number of args!");
        for (int i=0; i < more.length; i+=2)
            map.put((K) more[i], (V) more[i+1]);
        return map;
    }

    static <E> void shuffle(List<E> list) {
        final Random random = new Random();
        for (int i=0; i < list.size(); i++)
            swap(list, i, random.nextInt(list.size()));
    }

    static <E> void swap(List<E> list, int i, int j) {
        final E e = list.get(i);
        list.set(i, list.get(j));
        list.set(j, e);
    }
    
}
