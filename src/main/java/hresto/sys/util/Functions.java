package hresto.sys.util;

import java.util.function.UnaryOperator;

public final class Functions {

    public static <T> UnaryOperator<T> chain(UnaryOperator<T>... operators) {
        return t -> {
            T r = t;
            for (UnaryOperator<T> o : operators)
                r = o.apply(r);
            return r;
        };
    }

}
