package hresto.sys.util;

import jtree.util.io.TextInput;

import java.io.Reader;

public interface InputParser<C, T> {

    default T parse(C context, Reader reader) {
        return parse(context, TextInput.wrap(reader));
    }

    default T parse(C context, String text) {
        return parse(context, TextInput.wrap(text));
    }

    T parse(C context, TextInput input);

}
