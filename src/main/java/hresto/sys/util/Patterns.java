package hresto.sys.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Patterns {

    public static <T> Optional<T> applyIfMatched(Pattern pattern, String str, Function<Matcher, T> consumer) {
        final Matcher matcher;
        return (matcher = pattern.matcher(str)).matches() ? Optional.of(consumer.apply(matcher)) : Optional.empty();
    }

    public static Optional<String> replaceIfFound(Pattern pattern, String str, Function<Matcher, String> replacer) {
        final String result = replaceAll(pattern, str, replacer);
        return result.equals(str) ? Optional.empty() : Optional.of(result);
    }

    public static String replaceAll(Pattern pattern, String str, Function<Matcher, String> replacer) {
        final Matcher matcher = pattern.matcher(str);
        if (!matcher.find())
            return str;

        final StringBuilder sb = new StringBuilder();
        int last = 0;
        do {
            sb.append(str.substring(last, matcher.start())).append(replacer.apply(matcher));
            last = matcher.end();
        } while (matcher.find());

        if (last < str.length())
            sb.append(str.substring(last));

        return sb.toString();
    }

}
