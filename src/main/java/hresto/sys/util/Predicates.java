package hresto.sys.util;

import java.util.function.Predicate;

public interface Predicates {

    static <T> Predicate<T> not(Predicate<T> p) {
        return t -> !p.test(t);
    }

}
