package hresto.sys.util;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exceptions {

    public static Runnable unchecked(CheckedRunnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static <I,O> Function<I,O> unchecked(CheckedFunction<I,O> f) {
        return in -> {
            try {
                return f.apply(in);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }

    public static String formatStacktrace(Throwable throwable) {
        return Stream.of(throwable.getStackTrace()).map(traceElement -> "\t\t" + traceElement.getClassName()
                + '.' + traceElement.getMethodName() + '(' + traceElement.getLineNumber() + ")\n")
                .collect(Collectors.joining());
    }

    public interface CheckedRunnable {
        void run() throws Exception;
    }

    public interface CheckedFunction<I,O> {
        O apply(I in) throws Exception;
    }

}
