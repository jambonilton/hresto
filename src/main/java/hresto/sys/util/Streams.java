package hresto.sys.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Streams {


    public static <T> Stream<T> toStream(Iterable<? extends T> iterable) {
        return StreamSupport.stream((Spliterator<T>) iterable.spliterator(), false);
    }

    public static <T> Stream<T> toStream(Enumeration<? extends T> enumeration) {
        return toStream(new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return enumeration.hasMoreElements();
            }
            @Override
            public T next() {
                return enumeration.nextElement();
            }
        });
    }

    public static <T> Stream<T> toStream(Iterator<? extends T> iterator) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED), false);
    }

    /**
     * Uses flatMap's automatic autoClosing to close the stream.
     */
    public static <T> Stream<T> autoClosing(Stream<? extends T> stream) {
        return Stream.of(stream).flatMap(Function.identity());
    }

}
