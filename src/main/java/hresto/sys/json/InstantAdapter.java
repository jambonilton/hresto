package hresto.sys.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.Instant;

public class InstantAdapter extends TypeAdapter<Instant> {

    @Override
    public void write(JsonWriter writer, Instant time) throws IOException {
        writer.value(time == null ? System.currentTimeMillis() : time.toEpochMilli());
    }

    @Override
    public Instant read(JsonReader reader) throws IOException {
        return Instant.ofEpochMilli(reader.nextLong());
    }

}
