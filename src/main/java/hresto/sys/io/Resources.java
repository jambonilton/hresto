package hresto.sys.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.util.Collections;
import java.util.stream.Stream;

public class Resources {

    private static final int MAX_DEPTH = 100;

    public static String toString(String resource) {
        try (InputStream in = getResourceAsStream(resource)) {
            return ByteStreams.toString(in);
        } catch (IOException e) {
            throw new ResourceIOException(e);
        }
    }

    public static String toString(Path path) {
        try (InputStream in = Files.newInputStream(path)) {
            return ByteStreams.toString(in);
        } catch (IOException e) {
            throw new ResourceIOException(e);
        }
    }

    public static Stream<Path> getFilesInPackage(String path) {
        try {
            Path myPath = getPathForResource(path);
            return Files.walk(myPath, MAX_DEPTH)
                    .filter(Files::isRegularFile);
        } catch (IOException | URISyntaxException e) {
            throw new ResourceIOException(e);
        }
    }

    private static Path getPathForResource(String path) throws URISyntaxException, IOException {
        final URL url = ClassLoader.getSystemClassLoader().getResource(path);
        if (url == null)
            throw new ResourceIOException("File does not exist "+path);
        final URI uri = url.toURI();
        return uri.getScheme().equals("jar")
                ? getJarFileSystem(uri).getPath(path)
                : Paths.get(uri);
    }

    private static FileSystem jarFileSystem;

    /**
     * JAR files work by their own set of rules, so we gotta use a separate file system when traversing resources inside a runnable jar.
     */
    private synchronized static FileSystem getJarFileSystem(URI uri) throws IOException {
        return jarFileSystem == null
                ? jarFileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap())
                : jarFileSystem;
    }

    private static InputStream getResourceAsStream(String resource) {
        InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resource);
        if (in == null && (in = Resources.class.getResourceAsStream(resource)) == null)
            throw new ResourceIOException("Resource " + resource + " does not exist!");
        return in;
    }

    public static class ResourceIOException extends RuntimeException {

        public ResourceIOException(String message) {
            super(message);
        }

        public ResourceIOException(Throwable cause) {
            super(cause);
        }

    }

}
