package hresto.sys.io;

import java.io.File;
import java.util.stream.Stream;

public class FileUtil {

    public static void deleteRecursively(String f) {
        deleteRecursively(new File(f));
    }

    public static void deleteRecursively(File f) {
        if (f.isDirectory())
            Stream.of(f.listFiles()).forEach(FileUtil::deleteRecursively);
        if (!f.delete())
            throw new RuntimeException("Failed to delete file "+f.getName());
    }

}
