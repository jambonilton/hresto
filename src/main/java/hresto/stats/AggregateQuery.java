package hresto.stats;

import java.util.Optional;

public class AggregateQuery {

    String target;
    String key;
    GroupingFunction operation;
    Long bucketSize;

    public Optional<String> getTarget() {
        return Optional.ofNullable(target);
    }

    public AggregateQuery setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getKey() {
        return key;
    }

    public AggregateQuery setKey(String key) {
        this.key = key;
        return this;
    }

    public GroupingFunction getOperation() {
        return operation;
    }

    public AggregateQuery setOperation(GroupingFunction operation) {
        this.operation = operation;
        return this;
    }

    public boolean isBucketed() {
        return bucketSize != null;
    }

    public Long getBucketSize() {
        return bucketSize;
    }

    public AggregateQuery setBucketSize(Long bucketSize) {
        this.bucketSize = bucketSize;
        return this;
    }

}
