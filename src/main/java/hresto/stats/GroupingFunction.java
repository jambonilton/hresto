package hresto.stats;

public enum GroupingFunction {
    SUM, AVG, COUNT
}