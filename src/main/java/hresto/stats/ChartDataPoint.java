package hresto.stats;

public class ChartDataPoint {

    final Object key;
    final Number value;

    public ChartDataPoint(Object key, Number value) {
        this.key = key;
        this.value = value;
    }

    public Object getKey() {
        return key;
    }

    public Number getValue() {
        return value;
    }
}
