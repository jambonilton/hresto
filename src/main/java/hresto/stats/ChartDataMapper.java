package hresto.stats;

import hresto.sql.exec.SQLResultMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ChartDataMapper implements SQLResultMapper<Object[]> {

    @Override
    public Object[] map(ResultSet rs) throws SQLException {
        final Object value = rs.getObject(2);
        if (value == null)
            return new Object[]{rs.getObject(1), null};
        else if (value instanceof Number)
            return new Object[]{rs.getObject(1), value};
        else if (value instanceof Date)
            return new Object[]{rs.getObject(1), ((Date) value).getTime()};
        else
            throw new UnsupportedOperationException("Non-numeric type for aggregation "+value.getClass());
    }

}
