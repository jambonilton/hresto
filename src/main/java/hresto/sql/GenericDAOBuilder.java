package hresto.sql;

import hresto.auth.User;
import hresto.rest.BasicResource;
import hresto.sql.annotations.Table;
import hresto.sql.exec.SQLExecutor;
import hresto.sql.exec.SQLResultMapper;
import hresto.sql.gen.WhereClause;
import jtree.util.data.Pair;
import jtree.util.reflect.Conversion;
import jtree.util.reflect.Fields;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static hresto.sql.GenericDAO.CREATOR;
import static hresto.sql.gen.SQLQueryBuilder.eq;
import static java.util.stream.Collectors.toMap;

public class GenericDAOBuilder<E extends BasicResource> {

    protected SQLExecutor executor;
    protected String table;
    protected Field[] fields;
    protected SQLResultMapper<E> resultSetToObject;
    protected Function<E, Map<String, Object>> objectToMap;
    protected Function<E, Object[]> objectToRow;
    protected Function<User, WhereClause> accessConstraint;

    public GenericDAOBuilder(Class<E> type) {
        this.table = type.isAnnotationPresent(Table.class) ? type.getAnnotation(Table.class).value() : type.getSimpleName();
        this.fields = Fields.getAllFields(type)
                .toArray(Field[]::new);
        this.resultSetToObject = resultSetToObject(type);
        this.objectToMap = getMapFromObject(type);
        this.objectToRow = getRowFromObject(type);
    }

    public static <E extends BasicResource, Q> GenericDAOBuilder<E> forType(Class<E> type) {
        return new GenericDAOBuilder<>(type);
    }

    // TODO should match the "fields" field.
    private SQLResultMapper<E> resultSetToObject(Class<E> type) {
        try {
            final Class<?>[] fieldTypes = Fields.getAllFields(type)
                    .map(Field::getType)
                    .toArray(size -> new Class<?>[size]);
            final Type[] genericFieldTypes = Fields.getAllFields(type)
                    .map(Field::getGenericType)
                    .toArray(Type[]::new);
            final Constructor<E> constructor = type.getConstructor(fieldTypes);
            return rs -> {
                try {
                    final Object[] args = new Object[fieldTypes.length];
                    for (int i=0; i < args.length; i++)
                        args[i] = Conversion.convertTo(rs.getObject(i + 1), genericFieldTypes[i]);
                    return constructor.newInstance(args);
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException(e);
                }
            };
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    // TODO should match the "fields" field.
    private Function<E, Object[]> getRowFromObject(Class<E> type) {
        return instance ->
                Fields.getAllFields(type)
                        .map(f -> Fields.getValue(instance, f))
                        .toArray();
    }

    // TODO should match the "fields" field.
    private Function<E, Map<String, Object>> getMapFromObject(Class<E> type) {
        return instance ->
                Fields.getAllFields(type)
                        .map(f -> new Pair<>(f.getName(), Fields.getValue(instance, f)))
                        .filter(pair -> pair.getValue() != null)
                        .collect(toMap(p -> p.getKey(), p -> p.getValue()));
    }

    public GenericDAOBuilder withDatabase(SQLDatabase database) {
        return this.setExecutor(new SQLExecutor(database));
    }

    public GenericDAOBuilder setExecutor(SQLExecutor executor) {
        this.executor = executor;
        return this;
    }

    public GenericDAOBuilder setTable(String table) {
        this.table = table;
        return this;
    }

    public GenericDAOBuilder setFields(Field[] fields) {
        this.fields = fields;
        return this;
    }

    public GenericDAOBuilder setResultSetToObject(SQLResultMapper<E> resultSetToObject) {
        this.resultSetToObject = resultSetToObject;
        return this;
    }

    public GenericDAOBuilder setObjectToRow(Function<E, Object[]> objectToRow) {
        this.objectToRow = objectToRow;
        return this;
    }

    public GenericDAO<E> build() {
        return new GenericDAO<>(executor, table, fields, resultSetToObject, objectToMap, objectToRow, getAccessConstraint());
    }

    public GenericDAOBuilder setAccessConstraint(Function<User, WhereClause> accessConstraint) {
        this.accessConstraint = accessConstraint;
        return this;
    }

    public Function<User, WhereClause> getAccessConstraint() {
        return accessConstraint != null
                ? accessConstraint
                : Stream.of(fields).anyMatch(f -> f.getName().equals(CREATOR)) ? user -> eq(CREATOR, user.getId()) : null;
    }
}
