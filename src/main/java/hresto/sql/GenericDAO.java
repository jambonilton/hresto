package hresto.sql;

import hresto.auth.User;
import hresto.rest.BasicResource;
import hresto.rest.Resource;
import hresto.sql.exec.SQLExecutor;
import hresto.sql.exec.SQLResultMapper;
import hresto.sql.gen.SQLQueryBuilder;
import hresto.sql.gen.SQLQueryable;
import hresto.sql.gen.SQLUpdateBuilder;
import hresto.sql.gen.WhereClause;
import hresto.stats.AggregateQuery;
import hresto.stats.ChartDataMapper;
import hresto.sys.util.Arrays;
import jtree.query.Comparison;
import jtree.query.Query;
import jtree.query.Conjunction;
import jtree.query.Disjunction;

import java.lang.reflect.Field;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import static hresto.sql.gen.SQLDeleteBuilder.deleteFrom;
import static hresto.sql.gen.SQLInsertBuilder.insertInto;
import static hresto.sql.gen.SQLQueryBuilder.*;
import static hresto.sql.gen.SQLUpdateBuilder.update;
import static hresto.sys.util.Arrays.anyMatch;
import static hresto.sys.util.Arrays.find;
import static java.util.stream.Collectors.toList;

public class GenericDAO<E extends BasicResource> implements DAO<E> {

    public static final String ID = "id", CREATOR = "creator";

    protected final SQLExecutor executor;

    protected final String table;
    protected final Field[] fields;
    protected final SQLResultMapper<E> resultSetToObject;
    protected final Function<E, Map<String, Object>> objectToMap;
    protected final Function<E, Object[]> objectToRow;
    protected final Function<User, WhereClause> accessConstraint;

    public GenericDAO(SQLExecutor executor,
                      String table,
                      Field[] fields,
                      SQLResultMapper<E> resultSetToObject,
                      Function<E, Map<String, Object>> objectToMap,
                      Function<E, Object[]> objectToRow,
                      Function<User, WhereClause> accessConstraint) {
        this.executor = executor;
        this.table = table;
        this.fields = fields;
        this.resultSetToObject = resultSetToObject;
        this.objectToMap = objectToMap;
        this.objectToRow = objectToRow;
        this.accessConstraint = accessConstraint;
    }

    @Override
    public void save(User user, E item) {
        if (item.isNew()) {
            if (item instanceof Resource)
                ((Resource) item).setCreator(user.getId());
            item.setId(UUID.randomUUID());
            executor.apply(insertInto(table, getFieldNames())
                    .values(objectToRow.apply(item)));
        }
        else {
            final SQLUpdateBuilder u = update(table);
            objectToMap.apply(item).forEach(u::set);
            executor.apply(u.where(authd(eq(ID, item.getId()), user)));
        }
    }

    @Override
    public Stream<E> list(User user, Query query) {
        return executor.query(select(getFieldNames())
                .from(table)
                .where(authd(user, query)
            ), resultSetToObject);
    }

    private WhereClause toWhereClause(Query query) {
        if (query instanceof Conjunction)
            return and(query.stream().map(this::toWhereClause).collect(toList()));
        else if (query instanceof Disjunction)
            return or(query.stream().map(this::toWhereClause).collect(toList()));
        else if (query instanceof Comparison) {
            final Comparison comparison = (Comparison) query;
            switch (((Comparison) query).comparator()) {
                case equals: return eq(comparison.getKey(), comparison.getValue());
                case lessThan: return lessThan(comparison.getKey(), comparison.getValue());
                case lessThanOrEqual: return lessThanEq(comparison.getKey(), comparison.getValue());
                        case greaterThan: return greaterThan(comparison.getKey(), comparison.getValue());
                case greaterThanOrEqual: return greaterThanEq(comparison.getKey(), comparison.getValue());
                case isIn: return in(comparison.getKey(), (Collection<?>) comparison.getValue());
                case contains: return contains(comparison.getKey(), comparison.getValue());
                default: // TODO
            }
        }
        throw new IllegalArgumentException("Unsupported query "+query);
    }

    @Override
    public void remove(User user, UUID id) {
        executor.apply(deleteFrom(table).where(authd(eq(ID, id), user)));
    }

    @Override
    public E get(User user, UUID id) {
        final SQLQueryBuilder select = select(getFieldNames()).from(table).where(authd(eq(ID, id), user));
        return executor.query(select, resultSetToObject).findFirst().get();
    }

    @Override
    public Stream<E> get(User user, Collection<UUID> ids) {
        final SQLQueryBuilder select = select(getFieldNames()).from(table).where(authd(in(ID, ids), user));
        return executor.query(select, resultSetToObject);
    }

    private String[] getFieldNames() {
        return Arrays.map(Field::getName, String[]::new, fields);
    }

    @Override
    public Stream<Object[]> aggregate(User user, AggregateQuery aggregateQuery, Query query) {
        final SQLQueryable select = select(getKeySelect(aggregateQuery), getFieldWithOperation(aggregateQuery))
                .from(table)
                .where(authd(user, query))
                .groupBy(aggregateQuery.getKey());
        return executor.query(select, new ChartDataMapper());
    }

    private String getKeySelect(AggregateQuery aq) {
        String key = aq.getKey();
        if (!aq.isBucketed())
            return key;
        if (!canMath(key))
            key = tryMathFunction(key);
        return "("+aq.getBucketSize()+"*FLOOR("+ key +"/"+aq.getBucketSize()+")) as "+ aq.getKey() + "_bucket";
    }

    private String getFieldWithOperation(AggregateQuery aq) {
        return aq.getOperation() + "(" + aq.getTarget().orElse("*") + ") as "+aq.getTarget().orElse("the")+"_"+aq.getOperation();
    }

    private boolean canMath(String fieldName) {
        return anyMatch(fields, f -> f.getName().equals(fieldName) && Number.class.isAssignableFrom(f.getType()));
    }

    private String tryMathFunction(String fieldName) {
        final Class<?> type = find(fields, f -> f.getName().equals(fieldName))
                .map(Field::getType)
                .orElseThrow(IllegalArgumentException::new);
        if (Temporal.class.isAssignableFrom(type))
            return "UNIX_MILLIS("+fieldName+")";
        throw new IllegalArgumentException("Cannot perform math functions on field "+fieldName+" with type "+type.getSimpleName());
    }

    private boolean isAuthenticated() {
        return this.accessConstraint != null;
    }

    private WhereClause authd(User user, Query query) {
        if (query.isTautology())
            return isAuthenticated() ? authd(user) : null;
        final WhereClause where = toWhereClause(query);
        if (isAuthenticated())
            return and(authd(user), where);
        return where;
    }

    private WhereClause authd(User user) {
        return accessConstraint.apply(user);
    }

    private WhereClause authd(WhereClause where, User user) {
        if (isAuthenticated())
            return and(authd(user), where);
        return where;
    }

}
