package hresto.sql.hyper;

import hresto.sql.SQLDatabase;
import hresto.sql.exec.SQLRuntimeException;
import hresto.sys.io.Resources;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class AutoInitHSQLDatabase implements SQLDatabase {

    private final SQLDatabase base;
    private final String[] sqlScripts;

    public AutoInitHSQLDatabase(SQLDatabase base, String... sqlScripts) {
        this.base = base;
        this.sqlScripts = sqlScripts;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return base.getConnection();
    }

    @Override
    public boolean shouldRetry(SQLException e) {
        if (e.getMessage() != null && e.getMessage().contains("object not found"))
            return initTables();
        return base.shouldRetry(e);
    }

    boolean initTables() {
        try (final Connection conn = base.getConnection()) {
            try (final Statement st = conn.createStatement()) {
                for (String script : sqlScripts) {
                    try {
                        final String sql = Resources.toString(script);
                        st.execute(sql);
                    } catch (SQLException e) {
                        throw new SQLRuntimeException("Failed to execute script: "+script, e);
                    }
                }
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException("Failed to initialize database!", e);
        }
        return true;
    }

}
