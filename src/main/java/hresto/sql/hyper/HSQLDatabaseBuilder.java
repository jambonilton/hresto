package hresto.sql.hyper;

import hresto.sql.SQLDatabase;

import java.io.File;

public final class HSQLDatabaseBuilder {

    private final String file;
    private String[] scripts = new String[0];

    public HSQLDatabaseBuilder(String file) {
        this.file = file;
    }

    public HSQLDatabaseBuilder withScripts(String... scripts) {
        this.scripts = scripts;
        return this;
    }

    public SQLDatabase build() {
        final AutoInitHSQLDatabase db = new AutoInitHSQLDatabase(new HSQLFileDatabase(file), scripts);
        if (!new File(file).exists())
            db.initTables();
        return db;
    }

    public static HSQLDatabaseBuilder forFile(String file) {
        return new HSQLDatabaseBuilder(file);
    }

}
