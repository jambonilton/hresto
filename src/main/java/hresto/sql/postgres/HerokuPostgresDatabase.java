package hresto.sql.postgres;

import hresto.sql.SQLDatabase;
import hresto.sys.util.Primitives;
import org.postgresql.Driver;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.temporal.Temporal;
import java.util.UUID;

public class HerokuPostgresDatabase implements SQLDatabase {

    @Override
    public Connection getConnection() throws SQLException {
        try {
            Class.forName(Driver.class.getName()); // get driver into ClassLoader
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Could not find driver for database, cannot continue.", e);
        }
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        return DriverManager.getConnection(dbUrl);
    }

    @Override
    public boolean supports(Class<?> type) {
        return type.isPrimitive()
                || Primitives.isWrapperType(type)
                || String.class.isAssignableFrom(type)
                || Temporal.class.isAssignableFrom(type)
                || type.isEnum()
                || UUID.class.isAssignableFrom(type)
                || InetAddress.class.isAssignableFrom(type);
    }

}
