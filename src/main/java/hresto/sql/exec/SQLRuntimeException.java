package hresto.sql.exec;

import java.sql.SQLException;

public class SQLRuntimeException extends RuntimeException {

    public SQLRuntimeException(String message, SQLException cause) {
        super(message, cause);
    }

    public SQLRuntimeException(SQLException e) {
        super(e);
    }

    @Override
    public synchronized SQLException getCause() {
        return (SQLException) super.getCause();
    }

}
