package hresto.sql.exec;

import hresto.sql.SQLDatabase;
import hresto.sql.gen.SQLExecutable;
import hresto.sql.gen.SQLQueryable;
import jtree.util.reflect.Conversion;

import java.sql.*;
import java.time.Instant;
import java.util.function.Function;
import java.util.stream.Stream;

import static hresto.sys.util.Streams.autoClosing;
import static hresto.sys.util.Streams.toStream;

public class SQLExecutor {

    final SQLDatabase database;

    public SQLExecutor(SQLDatabase database) {
        this.database = database;
    }

    public <E> Stream<E> query(SQLQueryable queryable, SQLResultMapper<E> mapper) {

        try {
            final Connection conn = database.getConnection();
            final PreparedStatement preparedStatement = conn.prepareStatement(queryable.toPreparedString());
            for (int i = 0; i < queryable.getArguments().length; i++) {
                Object value = queryable.getArguments()[i];
                if (value != null && !database.supports(value.getClass()))
                    value = Conversion.convertTo(value, String.class);

                preparedStatement.setObject(i + 1, value);
            }
            final ResultSet resultSet = preparedStatement.executeQuery();
            final Stream<E> results = toStream(new ResultSetIterator(resultSet))
                    .onClose(() -> close(conn, preparedStatement, resultSet))
                    .map(wrap(mapper));
            return autoClosing(results);

        } catch (SQLException e) {
            throw new SQLRuntimeException("Failed to execute SQL: "+queryable.toPreparedString(), e);
        }
    }

    public int apply(SQLExecutable update) {
        try (final Connection conn = database.getConnection()) {
            try (final PreparedStatement ps = conn.prepareStatement(update.toPreparedString())) {
                for (int i=0; i < update.getArguments().length; i++) {
                    Object value = update.getArguments()[i];
                    if (value != null && !database.supports(value.getClass()))
                        value = Conversion.convertTo(value, String.class);
                    else if (value instanceof Instant)
                        value = new Timestamp(((Instant) value).toEpochMilli());
                    else if (value instanceof Enum)
                        value = ((Enum) value).ordinal();
                    ps.setObject(i+1, value);
                }
                return ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    private void close(Connection conn, PreparedStatement ps, ResultSet rs) {
        try {
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    private <E> Function<ResultSet, E> wrap(SQLResultMapper<E> mapper) {
        return resultSet -> {
            try {
                return mapper.map(resultSet);
            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            }
        };
    }

}
