package hresto.sql.exec;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQLResultMapper<E> {

    E map(ResultSet rs) throws SQLException;

}
