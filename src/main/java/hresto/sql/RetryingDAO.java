package hresto.sql;

import hresto.auth.User;
import hresto.sql.exec.SQLRuntimeException;
import hresto.stats.AggregateQuery;
import jtree.query.Query;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Stream;

public class RetryingDAO<E> implements DAO<E> {

    final DAO<E> base;
    final SQLDatabase database;

    public RetryingDAO(DAO<E> base, SQLDatabase database) {
        this.base = base;
        this.database = database;
    }

    @Override
    public void save(User user, E item) {
        try {
            base.save(user, item);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                save(user, item);
            throw e;
        }
    }

    @Override
    public Stream<E> list(User user, Query query) {
        try {
            return base.list(user, query);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                return list(user, query);
            throw e;
        }
    }

    @Override
    public E get(User user, UUID id) {
        try {
            return base.get(user, id);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                return get(user, id);
            throw e;
        }
    }

    @Override
    public Stream<E> get(User user, Collection<UUID> ids) {
        try {
            return base.get(user, ids);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                return get(user, ids);
            throw e;
        }
    }

    @Override
    public void remove(User user, UUID id) {
        try {
            base.remove(user, id);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                remove(user, id);
            throw e;
        }
    }

    @Override
    public Stream<Object[]> aggregate(User user, AggregateQuery aggregateQuery, Query query) {
        try {
            return base.aggregate(user, aggregateQuery, query);
        } catch (SQLRuntimeException e) {
            if (database.shouldRetry(e.getCause()))
                return aggregate(user, aggregateQuery, query);
            throw e;
        }
    }
}
