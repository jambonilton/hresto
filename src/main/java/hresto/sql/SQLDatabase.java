package hresto.sql;

import hresto.sys.util.Primitives;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.temporal.Temporal;

public interface SQLDatabase {

    Connection getConnection() throws SQLException;

    default boolean shouldRetry(SQLException e) {
        return false;
    }

    default boolean supports(Class<?> type) {
        return type.isPrimitive()
                || Primitives.isWrapperType(type)
                || String.class.isAssignableFrom(type)
                || Temporal.class.isAssignableFrom(type)
                || type.isEnum();
    }

}
