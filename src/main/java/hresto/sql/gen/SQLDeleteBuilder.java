package hresto.sql.gen;

public class SQLDeleteBuilder {

    public static SQLDeleteBuilder deleteFrom(String table) {
        return new SQLDeleteBuilder(table);
    }

    private final String table;

    SQLDeleteBuilder(String table) {
        this.table = table;
    }

    public SQLExecutable where(WhereClause where) {
        return new SQLExecutable() {
            @Override
            public String toPreparedString() {
                return "DELETE FROM "+table+ " WHERE " + where.toPreparedSQL();
            }
            @Override
            public Object[] getArguments() {
                return where.getArguments();
            }
            @Override
            public String toString() {
                return this.toPreparedString();
            }
        };
    }

}
