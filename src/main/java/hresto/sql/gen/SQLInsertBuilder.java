package hresto.sql.gen;

import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class SQLInsertBuilder {

    public static SQLInsertBuilder insertInto(String table, String... fields) {
        return new SQLInsertBuilder(table, fields);
    }

    private final String table;
    private final String[] fields;

    public SQLInsertBuilder(String table, String[] fields) {
        this.table = table;
        this.fields = fields;
    }

    public SQLExecutable values(Object... row) {
        return new SQLExecutable() {
            @Override
            public String toPreparedString() {
                return "INSERT INTO "+table+" ("+Stream.of(fields).collect(joining(", "))+") VALUES ("
                        +Stream.generate(()->"?").limit(fields.length).collect(joining(", "))+")";
            }
            @Override
            public Object[] getArguments() {
                return row;
            }
            @Override
            public String toString() {
                return this.toPreparedString();
            }
        };
    }

}
