package hresto.sql.gen;

public interface SQLExecutable {

    String toPreparedString();

    Object[] getArguments();

}
