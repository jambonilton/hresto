package hresto.sql.gen;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class SQLUpdateBuilder {

    public static SQLUpdateBuilder update(String table) {
        return new SQLUpdateBuilder(table);
    }

    private final String table;
    private final Map<String, Object> assignments;

    SQLUpdateBuilder(String table) {
        this.table = table;
        this.assignments = new HashMap<>();
    }

    public SQLUpdateBuilder set(String key, Object value) {
        assignments.put(key, value);
        return this;
    }

    public SQLExecutable where(WhereClause where) {
        return new SQLExecutable() {
            @Override
            public String toPreparedString() {
                return "UPDATE "+table+" SET "
                        + assignments.entrySet().stream().map(e -> e.getKey()+"=?").collect(Collectors.joining(", "))
                        + " WHERE " + where.toPreparedSQL();
            }
            @Override
            public Object[] getArguments() {
                final Collection<Object> assignmentValues = assignments.values();
                final Object[] whereValues = where.getArguments();
                final Object[] args = new Object[assignmentValues.size()+whereValues.length];
                int i=0;
                for (Object arg : assignmentValues)
                    args[i++] = arg;
                System.arraycopy(whereValues, 0, args, i, whereValues.length);
                return args;
            }
            @Override
            public String toString() {
                return this.toPreparedString();
            }
        };
    }


}
