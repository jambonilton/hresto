package hresto.sql.gen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;

public class SQLQueryBuilder extends AbstractSQLQueryable implements SQLQueryable
{

    public static SQLQueryBuilder.Fields select(String... fields)
    {
        return new SQLQueryBuilder(fields).fields;
    }

    public static SQLQueryable union(SQLQueryable... subQueries)
    {
        return union(asList(subQueries));
    }

    public static SQLQueryable union(Collection<SQLQueryable> subQueries)
    {
        if (subQueries.isEmpty())
            throw new IllegalArgumentException("Zero subqueries supplied for union! At least 1 required.");
        if (subQueries.size() == 1)
            return subQueries.iterator().next();
        return new UnionedSQLQueryable(subQueries);
    }

    private final Fields fields;
    private Tables tables;
    private WhereClause where;
    private String[] groupBy;

    private SQLQueryBuilder(String[] fields)
    {
        this.fields = new Fields(fields);
    }

    private SQLQueryBuilder.Tables setTables(String table)
    {
        this.tables = new Tables(table);
        return tables;
    }

    private SQLQueryBuilder setWhere(WhereClause[] clauses)
    {
        if (clauses.length == 0)
            return this;
        this.where = and(clauses);
        return this;
    }

    @Override
    public SQLQueryBuilder groupBy(String... groupByColumns) {
        this.groupBy = groupByColumns;
        return this;
    }

    @Override
    public String toString()
    {
        return toPreparedString();
    }

    @Override
    public String toPreparedString()
    {
        String sql = "SELECT " + fields.toSQL() + " FROM " + tables.toSQL();
        if (where != null)
            sql += " WHERE " + where.toPreparedSQL();
        if (groupBy != null)
            sql += " GROUP BY " + Stream.of(groupBy).collect(joining(", "));
        return sql + super.getPagingString();
    }

    @Override
    public Object[] getArguments()
    {
        if (where == null)
            return new Object[0];
        return where.getArguments();
    }

    /**
     * Initial state of query builder, allows only for setting tables.
     */
    public class Fields
    {
        private String[] fields;

        public Fields(String[] fields)
        {
            this.fields = fields;
        }

        public SQLQueryBuilder.Tables from(String table)
        {
            return setTables(table);
        }

        private String toSQL()
        {
            if (fields.length == 0)
                return "*";
            return Stream.of(fields).collect(joining(", "));
        }

    }

    /**
     * Second state, allows for additional joins, where clause, and delegate methods to enclosing class.
     */
    public class Tables implements SQLQueryable
    {

        private final String table;
        private List<Join> joins = new ArrayList<>();


        public Tables(String table)
        {
            this.table = table;
        }

        public Tables join(String other, String column)
        {
            joins.add(new JoinUsing(other, column));
            return this;
        }

        public Tables join(String other, String leftColumn, String rightColumn)
        {
            joins.add(new JoinOn(null, table, other, leftColumn, rightColumn));
            return this;
        }

        public Tables leftJoin(String other, String leftColumn, String rightColumn)
        {
            joins.add(new JoinOn("LEFT", table, other, leftColumn, rightColumn));
            return this;
        }

        public Tables innerJoin(String other, String leftColumn, String rightColumn)
        {
            joins.add(new JoinOn("INNER", table, other, leftColumn, rightColumn));
            return this;
        }

        public SQLQueryBuilder where(WhereClause... clauses)
        {
            return setWhere(clauses);
        }

        public SQLQueryable orderBy(String orderBy)
        {
            return SQLQueryBuilder.this.orderBy(orderBy);
        }

        public SQLQueryable orderByDescending(String orderBy)
        {
            return SQLQueryBuilder.this.orderByDescending(orderBy);
        }

        @Override
        public SQLQueryable groupBy(String... groupBy) {
            return SQLQueryBuilder.this.groupBy(groupBy);
        }

        public SQLQueryable limit(Integer limit)
        {
            return SQLQueryBuilder.this.limit(limit);
        }

        public SQLQueryable offset(Integer offset)
        {
            return SQLQueryBuilder.this.offset(offset);
        }

        @Override
        public Integer getOffset()
        {
            return SQLQueryBuilder.this.getOffset();
        }

        @Override
        public Integer getLimit()
        {
            return SQLQueryBuilder.this.getLimit();
        }

        private String toSQL()
        {
            if (joins.isEmpty())
                return table;
            return table + ' ' + joins.stream().map(Join::toSQL).collect(joining(" "));
        }

        @Override
        public String toPreparedString()
        {
            return SQLQueryBuilder.this.toPreparedString();
        }

        @Override
        public Object[] getArguments()
        {
            return SQLQueryBuilder.this.getArguments();
        }

        @Override
        public String toString()
        {
            return SQLQueryBuilder.this.toString();
        }
    }

    private static abstract class Join
    {

        final String modifier;
        final String leftTable;
        final String table;
        final String leftColumn;

        public Join(String modifier, String leftTable, String table, String leftColumn)
        {
            this.modifier = modifier;
            this.leftTable = leftTable;
            this.table = table;
            this.leftColumn = leftColumn;
        }

        abstract String toSQL();

    }

    private static class JoinUsing extends Join
    {

        public JoinUsing(String table, String column)
        {
            super(null, null, table, column);
        }

        @Override
        String toSQL()
        {
            return "JOIN " + table + " USING (" + leftColumn + ")";
        }

    }

    private static class JoinOn extends Join
    {

        final String rightColumn;

        public JoinOn(String modifier, String leftTable, String table, String leftColumn, String rightColumn)
        {
            super(modifier, leftTable, table, leftColumn);
            this.rightColumn = rightColumn;
        }

        @Override
        String toSQL()
        {
            final String leftTableName = getNameOfTable(this.leftTable),
                         rightTableName = getNameOfTable(this.table);
            return (modifier != null ? modifier + " " : "")
                    + "JOIN " + table + " ON (" + leftTableName +'.'+leftColumn + " = " + rightTableName+'.'+rightColumn + ")";
        }

        private String getNameOfTable(String table) {
            final int space = table.lastIndexOf(' ');
            return space == -1 ? table : table.substring(space+1);
        }

    }

    public static WhereClause and(WhereClause... clauses)
    {
        return and(asList(clauses));
    }

    public static WhereClause and(List<WhereClause> clauses)
    {
        if (clauses.isEmpty())
            throw new IllegalArgumentException("Cannot join empty set of clauses!");
        if (clauses.size() == 1)
            return clauses.get(0);
        return new Conjunction(clauses);
    }

    public static WhereClause or(WhereClause... clauses)
    {
        return or(asList(clauses));
    }

    public static WhereClause or(List<WhereClause> clauses)
    {
        if (clauses.isEmpty())
            throw new IllegalArgumentException("Cannot join empty set of clauses!");
        if (clauses.size() == 1)
            return clauses.get(0);
        return new Disjunction(clauses);
    }

    public static WhereClause eq(String field, Object value)
    {
        return value == null ? isNull(field) : new ComparisonClause(field, ComparisonOperator.EQUALS, value);
    }

    public static WhereClause in(String field, Collection<?> value)
    {
        if (value == null || value.isEmpty())
            return never();
        return new InClause(field, value);
    }

    public static WhereClause contains(String field, Object value)
    {
        return new ContainsClause(field, value);
    }

    public static WhereClause isNull(String field)
    {
        return new IsNullClause(field);
    }

    public static WhereClause lessThan(String field, Object value)
    {
        return new ComparisonClause(field, ComparisonOperator.LESS_THAN, value);
    }

    public static WhereClause lessThanEq(String field, Object value)
    {
        return new ComparisonClause(field, ComparisonOperator.LESS_THAN_EQUALS, value);
    }

    public static WhereClause greaterThan(String field, Object value)
    {
        return new ComparisonClause(field, ComparisonOperator.GREATER_THAN, value);
    }

    public static WhereClause greaterThanEq(String field, Object value)
    {
        return new ComparisonClause(field, ComparisonOperator.GREATER_THAN_EQUALS, value);
    }

    public static WhereClause like(String field, String value)
    {
        return new ComparisonClause(field, ComparisonOperator.LIKE, value);
    }

    public static WhereClause never()
    {
        return new WhereClause() {
            @Override
            public String toPreparedSQL() {
                return "false";
            }
            @Override
            public Object[] getArguments() {
                return new Object[0];
            }
        };
    }

    private static class JoinedClause implements WhereClause
    {

        final List<WhereClause> terms;
        final String joiner;

        public JoinedClause(List<WhereClause> terms, String joiner)
        {
            this.terms = terms;
            this.joiner = joiner;
        }

        @Override
        public String toPreparedSQL()
        {
            return terms.stream()
                    .map(t -> t instanceof JoinedClause ? "(" + t.toPreparedSQL() + ")" : t.toPreparedSQL())
                    .collect(joining(joiner));
        }

        @Override
        public Object[] getArguments()
        {
            return terms.stream()
                    .map(WhereClause::getArguments)
                    .flatMap(Stream::of)
                    .toArray();
        }
    }

    private static class Conjunction extends JoinedClause
    {
        public Conjunction(List<WhereClause> terms)
        {
            super(terms, " AND ");
        }
    }

    private static class Disjunction extends JoinedClause
    {
        public Disjunction(List<WhereClause> terms)
        {
            super(terms, " OR ");
        }
    }

    private static class ComparisonClause implements WhereClause
    {

        final String field;
        final String operator;
        final Object value;

        ComparisonClause(String field, ComparisonOperator operator, Object value)
        {
            this(field, operator.toString(), value);
        }

        ComparisonClause(String field, String operator, Object value)
        {
            this.field = field;
            this.operator = operator;
            this.value = value;
        }

        @Override
        public String toPreparedSQL()
        {
            return field + ' ' + operator + " ?";
        }

        @Override
        public Object[] getArguments()
        {
            return new Object[]{value};
        }
    }

    private static class InClause implements WhereClause
    {

        final String field;
        final Collection<?> value;

        public InClause(String field, Collection<?> value)
        {
            this.field = field;
            this.value = value;
        }

        @Override
        public String toPreparedSQL()
        {
            return field + " IN (" + value.stream().map(e -> "?").collect(joining(",")) + ")";
        }

        @Override
        public Object[] getArguments()
        {
            return value.toArray();
        }

    }

    private static class ContainsClause implements WhereClause
    {

        final String field;
        final Object value;

        public ContainsClause(String field, Object value)
        {
            this.field = field;
            this.value = value;
        }

        @Override
        public String toPreparedSQL()
        {
            return "LOWER("+field+") LIKE LOWER(?)";
        }

        @Override
        public Object[] getArguments()
        {
            return new Object[]{'%'+String.valueOf(value)+'%'};
        }

    }

    private static class IsNullClause implements WhereClause
    {

        final String field;

        public IsNullClause(String field)
        {
            this.field = field;
        }

        @Override
        public String toPreparedSQL()
        {
            return field + " IS NULL";
        }

        @Override
        public Object[] getArguments()
        {
            return new Object[0];
        }

    }

    private enum ComparisonOperator
    {
        EQUALS("="),
        LESS_THAN("<"),
        LESS_THAN_EQUALS("<="),
        GREATER_THAN(">"),
        GREATER_THAN_EQUALS(">="),
        LIKE("LIKE");

        String stringValue;

        ComparisonOperator(String stringValue)
        {
            this.stringValue = stringValue;
        }

        @Override
        public String toString()
        {
            return stringValue;
        }
    }

}
