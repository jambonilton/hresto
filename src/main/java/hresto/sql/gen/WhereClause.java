package hresto.sql.gen;

public interface WhereClause
{

    String toPreparedSQL();

    Object[] getArguments();

}
