package hresto.sql.gen;

public interface SQLQueryable
{

    String toPreparedString();

    Object[] getArguments();

    Integer getLimit();

    Integer getOffset();

    SQLQueryable orderBy(String orderBy);

    SQLQueryable orderByDescending(String orderBy);

    SQLQueryable limit(Integer limit);

    SQLQueryable offset(Integer offset);

    SQLQueryable groupBy(String... groupBy);

}
