package hresto.sql.gen;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class UnionedSQLQueryable extends AbstractSQLQueryable
{

    final Collection<SQLQueryable> subQueries;

    public UnionedSQLQueryable(Collection<SQLQueryable> subQueries)
    {
        if (subQueries.size() <= 1)
            throw new IllegalArgumentException("Invalid number of queries for unioned queryable: " + subQueries.size());
        this.subQueries = subQueries;
    }

    @Override
    public String toPreparedString()
    {
        final String sql = subQueries.stream()
                .map(subQuery -> '(' + subQuery.toPreparedString() + ')')
                .collect(Collectors.joining(" UNION "));
        return sql + super.getPagingString();
    }

    @Override
    public Object[] getArguments()
    {
        return subQueries.stream()
                .map(SQLQueryable::getArguments)
                .flatMap(Stream::of)
                .toArray();
    }
}
