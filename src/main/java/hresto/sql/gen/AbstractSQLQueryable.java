package hresto.sql.gen;

public abstract class AbstractSQLQueryable implements SQLQueryable
{

    protected Integer limit;
    protected Integer offset;
    protected String orderBy;
    protected String[] groupBy;
    protected Boolean descending;

    public SQLQueryable groupBy(String... groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public SQLQueryable limit(Integer limit)
    {
        this.limit = limit;
        return this;
    }

    public SQLQueryable offset(Integer offset)
    {
        this.offset = offset;
        return this;
    }

    public Integer getOffset()
    {
        return offset;
    }

    public Integer getLimit()
    {
        return limit;
    }

    public SQLQueryable orderBy(String orderBy)
    {
        this.orderBy = orderBy;
        return this;
    }

    public SQLQueryable orderByDescending(String orderBy)
    {
        this.descending = true;
        return this.orderBy(orderBy);
    }

    public String getPagingString()
    {
        StringBuilder sql = new StringBuilder();
        if (orderBy != null)
        {
            sql.append(" ORDER BY " + orderBy);
            if (descending != null)
                sql.append(descending ? " DESC" : " ASC");
        }
        if (limit != null)
            sql.append(" LIMIT " + limit);
        if (offset != null)
            sql.append(" OFFSET " + offset);
        return sql.toString();
    }

}
