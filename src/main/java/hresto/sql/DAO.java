package hresto.sql;

import hresto.auth.User;
import hresto.stats.AggregateQuery;
import jtree.query.Query;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Stream;

public interface DAO<E> {

    void save(User user, E item);

    Stream<E> list(User user, Query query);

    void remove(User user, UUID id);

    E get(User user, UUID id);

    Stream<E> get(User user, Collection<UUID> ids);

    Stream<Object[]> aggregate(User user, AggregateQuery aggregateQuery, Query query);

}
