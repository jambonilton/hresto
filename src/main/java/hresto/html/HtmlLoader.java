package hresto.html;

import hresto.sys.io.Resources;
import hresto.sys.util.Patterns;

import java.util.regex.Pattern;

import static hresto.sys.util.string.StringOperations.join;

// Recursively loads html using the "includes" tag.
public class HtmlLoader {

    private final static Pattern INCLUDES_PATTERN = Pattern.compile("<include\\s+src\\s*=\\s*\"([^\"]+)\"\\s*/>");

    private final String dir;

    public HtmlLoader(String dir) {
        this.dir = dir;
    }

    public String load(String file) {
        return Patterns.replaceAll(INCLUDES_PATTERN, Resources.toString(join('/', dir, file)), matcher -> load(matcher.group(1)));
    }

}
