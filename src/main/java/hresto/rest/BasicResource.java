package hresto.rest;

import java.util.UUID;

public abstract class BasicResource {

    protected UUID id;

    public BasicResource(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public boolean isNew() {
        return id == null;
    }

}