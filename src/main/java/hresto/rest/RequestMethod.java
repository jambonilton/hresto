package hresto.rest;

public enum RequestMethod {
    GET, POST, DELETE, PUT, HEAD
}
