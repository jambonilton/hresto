package hresto.rest;

import hresto.auth.SessionStore;
import hresto.auth.User;
import hresto.log.Logger;
import hresto.sql.GenericDAO;
import hresto.sql.GenericDAOBuilder;
import hresto.sql.SQLDatabase;
import hresto.sql.gen.WhereClause;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.BlockingHandler;
import jtree.util.io.json.Json;

import java.util.concurrent.ExecutorService;
import java.util.function.Function;

public class RestHandlerFactory {

    protected final ExecutorService exec;
    protected final Json serializer;
    protected final Logger logger;
    protected final SessionStore sessionStore;
    protected final SQLDatabase database;
    protected Function<User, WhereClause> accessConstraint;

    public RestHandlerFactory(ExecutorService exec,
                              Json serializer,
                              Logger logger,
                              SessionStore sessionStore,
                              SQLDatabase database) {
        this.exec = exec;
        this.serializer = serializer;
        this.logger = logger;
        this.sessionStore = sessionStore;
        this.database = database;
    }

    public RestHandlerFactory withAccessConstraint(Function<User, WhereClause> accessConstraint) {
        this.accessConstraint = accessConstraint;
        return this;
    }

    public HttpHandler create(String path, Class<? extends BasicResource> type) {
        return new BlockingHandler(new RestfulHttpHandler<>(logger, sessionStore, path,
                getDAO(type),
                new RestRequestInterpreter(serializer, type), exec));
    }

    public HttpHandler create(String path, RestRequestInterpreter<? extends BasicResource> interpreter) {
        return new BlockingHandler(new RestfulHttpHandler<>(logger, sessionStore, path,
                getDAO(interpreter.elementType),
                interpreter, exec));
    }

    private GenericDAO getDAO(Class<? extends BasicResource> type) {
        return GenericDAOBuilder.forType(type)
                .setAccessConstraint(accessConstraint)
                .withDatabase(database)
                .build();
    }

}
