package hresto.rest;

import java.util.UUID;

public class Resource extends BasicResource {

    protected UUID creator;

    public Resource(UUID id, UUID creator) {
        super(id);
        this.creator = creator;
    }

    public UUID getCreator() {
        return creator;
    }

    public Resource setCreator(UUID creator) {
        this.creator = creator;
        return this;
    }

}
