package hresto.rest;

import hresto.auth.Session;
import hresto.auth.SessionStore;
import hresto.auth.User;
import hresto.log.Logger;
import hresto.log.Severity;
import hresto.sql.DAO;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.Headers;
import jtree.util.io.json.Json;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

import static hresto.sys.util.string.StringOperations.removePrefix;
import static hresto.sys.util.string.StringOperations.trim;

public class RestfulHttpHandler<E extends BasicResource> implements HttpHandler {

    protected final Logger logger;
    protected final SessionStore sessionStore;
    protected final DAO<E> dao;
    protected final String basePath;
    protected final RestRequestInterpreter requestInterpreter;
    protected final ExecutorService exec;

    public RestfulHttpHandler(Logger logger, SessionStore sessionStore, String basePath, DAO<E> dao, RestRequestInterpreter requestInterpreter, ExecutorService exec) {
        this.logger = logger;
        this.sessionStore = sessionStore;
        this.dao = dao;
        this.basePath = basePath;
        this.requestInterpreter = requestInterpreter;
        this.exec = exec;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            final RequestMethod method = RequestMethod.valueOf(exchange.getRequestMethod().toString());
            logger.log(method.toString() + " " + exchange.getRequestPath());
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");

            final Supplier<Reader> reader;
            if (method.equals(RequestMethod.POST) || method.equals(RequestMethod.PUT)) {
                exchange.startBlocking();
                reader = () -> new InputStreamReader(exchange.getInputStream());
            } else {
                reader = () -> { throw new IllegalArgumentException("Only accepting response bodies on POST and PUT calls."); };
            }
            final Writer writer = new OutputStreamWriter(exchange.getOutputStream());
            final String path = trim(removePrefix(exchange.getRequestPath(), basePath), ch -> ch == '/');
            final Cookie sessionCookie = exchange.getRequestCookies().get("Session");
            if (sessionCookie == null)
                throw new IllegalAccessException("Unauthorized access to data endpoint.  Session header required.");
            final User user = sessionStore.get(sessionCookie.getValue()).map(Session::getUser)
                    .orElseThrow(() -> new IllegalAccessException("No session available for provided token."));
            requestInterpreter.interpret(user, method, path, exchange.getQueryParameters()).apply(dao, reader, writer);
            writer.close();
        } catch (Exception e) {
            logger.error(Severity.MAJOR, e, "Failed REST call, sending error info to client.");
            exchange.setStatusCode(getStatusCode(e));
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
            exchange.getResponseSender().send(e.getClass().getSimpleName() + "\n" + e.getMessage());
        }
    }

    private int getStatusCode(Exception e) {
        if (e instanceof IllegalArgumentException)
            return 400;
        else if (e instanceof IllegalAccessException)
            return 401;
        else if (e instanceof SocketTimeoutException)
            return 503;
        return 500;
    }

}
