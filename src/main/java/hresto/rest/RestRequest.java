package hresto.rest;

import hresto.sql.DAO;

import java.io.Reader;
import java.io.Writer;
import java.util.function.Supplier;

public interface RestRequest {

    void apply(DAO dao, Supplier<Reader> reader, Writer out);

}
