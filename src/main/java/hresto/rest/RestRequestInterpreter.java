package hresto.rest;

import hresto.auth.User;
import hresto.http.handlers.UrlFormReflection;
import hresto.stats.AggregateQuery;
import hresto.stats.GroupingFunction;
import jtree.query.Query;
import jtree.util.io.json.Json;

import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static hresto.sys.util.string.StringOperations.isEmpty;
import static java.util.stream.Collectors.toList;

public class RestRequestInterpreter<E extends Resource> {

    private static final long DEFAULT_LIMIT = 1000;

    static final Pattern
            UUID_PATTERN = Pattern.compile("[a-f0-9\\-]+"),
            MULTI_UUID_PATTERN = Pattern.compile("[a-f0-9\\-,]+"),
            AGGREGATE_PATTERN = Pattern.compile("(\\p{Alpha}+)(?:_(\\p{Alpha}*))?_by_(\\p{Alpha}+)(\\d*)");

    protected final Json json;
    protected final Class<E> elementType;
    protected final Function<User, Function<Map<String, Deque<String>>, Query>> queryInterpreter;

    public RestRequestInterpreter(Json json, Class<E> elementType) {
        this(json, elementType, user -> UrlFormReflection::getQueryObject);
    }

    public RestRequestInterpreter(Json json, Class<E> elementType, Function<User, Function<Map<String, Deque<String>>, Query>> queryInterpreter) {
        this.json = json;
        this.elementType = elementType;
        this.queryInterpreter = queryInterpreter;
    }

    public RestRequest interpret(User user, RequestMethod method, String path, Map<String, Deque<String>> parameters) {
        switch (method) {
            case GET:
                return getCall(user, path, parameters);
            case PUT:
            case POST:
                return writeCall(user, path);
            case DELETE:
                return deleteCall(user, path);
            case HEAD:
            default:
                throw new UnsupportedOperationException();
        }
    }

    private RestRequest getCall(User user, String path, Map<String, Deque<String>> parameters) {
        Matcher matcher;
        if (UUID_PATTERN.matcher(path).matches())
            return (dao, reader, writer) -> json.write(dao.get(user, UUID.fromString(path)), writer);
        else if (MULTI_UUID_PATTERN.matcher(path).matches())
            return (dao, reader, writer) -> json.write(dao.get(user, Stream.of(path.split(",")).map(UUID::fromString).collect(toList())).collect(toList()), writer);
        else if ((matcher = AGGREGATE_PATTERN.matcher(path)).matches())
            return (dao, reader, writer) -> {
                final Query queryFromParams = queryInterpreter.apply(user).apply(parameters);
                final AggregateQuery aggregateQuery = new AggregateQuery()
                        .setOperation(GroupingFunction.valueOf(matcher.group(1).toUpperCase()))
                        .setTarget(matcher.group(2))
                        .setKey(matcher.group(3))
                        .setBucketSize(isEmpty(matcher.group(4)) ? null : Long.parseLong(matcher.group(4)));
                final Object[] header = { aggregateQuery.getKey(), aggregateQuery.getOperation().toString().toLowerCase() };
                final Stream.Builder<Object[]> resultBuilder = Stream.builder();
                resultBuilder.add(header);
                dao.aggregate(user, aggregateQuery, queryFromParams).forEach(resultBuilder);
                final List<Object[]> results = resultBuilder.build().collect(toList());
                json.write(results, writer);
            };
        else if (path.isEmpty())
            return (dao, reader, writer) -> json.write(dao.list(user, queryInterpreter.apply(user).apply(parameters))
                    .limit(DEFAULT_LIMIT)
                    .collect(toList()), writer);
        throw new IllegalArgumentException("Unsupported path "+path);
    }

    private RestRequest writeCall(User user, String path) {
        return (dao, reader, writer) -> {
            final E resource = json.forType(elementType).parse(reader.get());
            resource.setCreator(user.getId());
            dao.save(user, resource);
            json.write(resource, writer);
        };
    }

    private RestRequest deleteCall(User user, String path) {
        return (dao, reader, writer) -> {
            if (UUID_PATTERN.matcher(path).matches())
                dao.remove(user, UUID.fromString(path));
            else
                throw new IllegalArgumentException("Expected ID but path was "+path);
        };
    }

}
