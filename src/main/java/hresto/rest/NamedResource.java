package hresto.rest;

import java.time.Instant;
import java.util.UUID;

public class NamedResource extends Resource {

    protected String name;
    protected Instant lastModified;

    public NamedResource(UUID id, UUID creator, String name, Instant lastModified) {
        super(id, creator);
        this.name = name;
        this.lastModified = lastModified;
    }

    public String getName() {
        return name == null ? getDefaultName() : name;
    }

    public Instant getLastModified() {
        return lastModified == null ? lastModified = Instant.now() : lastModified;
    }

    protected String getDefaultName() {
        return "Untitled";
    }

}
