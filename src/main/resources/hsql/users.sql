CREATE TABLE AppUser (
    id varchar(36) PRIMARY KEY,
    externalId varchar(64) DEFAULT NULL,
    name varchar(128) NOT NULL,
    email varchar(256) NOT NULL,
    photoUrl varchar(512),
    locale varchar(128),
    familyName varchar(128),
    givenName varchar(128)
);