package hresto.auth;

import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class StaticLengthRandomStringGeneratorTest {

    StaticLengthRandomStringGenerator gen = new StaticLengthRandomStringGenerator(100);

    @Test
    public void returnsRandomStringsOfStaticLength() throws Exception {
        final int expectedCount = 100;
        assertEquals(expectedCount, Stream.generate(gen::get).limit(expectedCount).distinct().count());
    }

}