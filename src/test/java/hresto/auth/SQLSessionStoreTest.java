package hresto.auth;

import hresto.sql.DAO;
import hresto.sql.GenericDAOBuilder;
import hresto.sql.SQLDatabase;
import hresto.sql.exec.SQLExecutor;
import hresto.sql.hyper.HSQLDatabaseBuilder;
import hresto.sys.io.FileUtil;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class SQLSessionStoreTest {

    static final String externalId = "externalId", email = "email", name = "name", photoUrl = "photoUrl",
                        locale = "locale", familyName = "familyName", givenName = "givenName";
    public static final String TEST_DB = "unit-test" + new StaticLengthRandomStringGenerator(5).get();


    static SQLDatabase database;
    DAO<User> userStore;
    SQLSessionStore sessionStore;

    @BeforeClass
    public static void initDB() {
        database = HSQLDatabaseBuilder.forFile(TEST_DB)
                .withScripts("hsql/users.sql", "hsql/sessions.sql").build();
    }

    @AfterClass
    public static void deleteFiles() {
        FileUtil.deleteRecursively(TEST_DB);
    }


    @Before
    public void setUp() {
        final SQLExecutor exec = new SQLExecutor(database);
        this.sessionStore = new SQLSessionStore(exec);
        this.userStore = GenericDAOBuilder.forType(User.class).setExecutor(exec).build();
    }

    @Test
    public void createAndGet() {
        final User user = new User(null, externalId, email, name, photoUrl, locale, familyName, givenName);
        userStore.save(User.GOD_MODE, user);
        assertNotNull(userStore.get(User.GOD_MODE, user.getId()));
        final Session session = sessionStore.create(user);
        assertEquals(user, session.getUser());
        final Optional<Session> sessionOptional = sessionStore.get(session.getId());
        assertTrue(sessionOptional.isPresent());
        assertEquals(session.getId(), sessionOptional.get().getId());
        assertEquals(session.getUser().getId(), sessionOptional.get().getUser().getId());
    }

}