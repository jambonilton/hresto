package hresto.rest;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertTrue;

public class RestRequestInterpreterTest {

    @Test
    public void test() {
        final String input = UUID.randomUUID().toString();
        assertTrue(RestRequestInterpreter.UUID_PATTERN.matcher(input).matches());
        assertTrue(RestRequestInterpreter.MULTI_UUID_PATTERN.matcher(input+","+UUID.randomUUID()).matches());
    }

}