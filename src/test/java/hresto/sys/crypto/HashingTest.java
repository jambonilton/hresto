package hresto.sys.crypto;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class HashingTest {

    @Test
    public void hash() {
        final String hash1 = Hashing.hash("this is a test");
        assertEquals("2e99758548972a8e8822ad47fa1017ff72f06f3ff6a016851f45c398732bc50c", hash1);
        assertNotSame(hash1, Hashing.hash("another test"));
    }

}