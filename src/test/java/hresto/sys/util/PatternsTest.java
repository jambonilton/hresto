package hresto.sys.util;

import org.junit.Test;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PatternsTest {

    final Function<Matcher, String> function = matcher -> reverse(matcher.group());

    @Test
    public void applyIfMatched() {
        final Pattern pattern = Pattern.compile("^this is a test$");
        assertFalse(Patterns.applyIfMatched(pattern, "", function).isPresent());
        assertEquals("tset a si siht", Patterns.applyIfMatched(pattern, "this is a test", function).get());
    }

    @Test
    public void replaceIfFound() {
        final Pattern pattern = Pattern.compile("\\w+");
        assertFalse(Patterns.replaceIfFound(pattern, "", function).isPresent());
        assertFalse(Patterns.replaceIfFound(pattern, "!!!!", function).isPresent());
        assertEquals("siht si a tset", Patterns.replaceIfFound(pattern, "this is a test", function).get());
    }

    private String reverse(String s) {
        final char[] reversed = new char[s.length()];
        for (int i=0, j=s.length()-1; i <= s.length()/2; i++, j--) {
            reversed[i] = s.charAt(j);
            reversed[j] = s.charAt(i);
        }
        return new String(reversed);
    }

}