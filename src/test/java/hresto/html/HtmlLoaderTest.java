package hresto.html;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HtmlLoaderTest {

    final HtmlLoader loader = new HtmlLoader("hresto/html");

    @Test
    public void test() {
        String expected = "ZOMBO.COM\n" +
                        "This is a test.\n" +
                        "|======= |\n" +
                        "|===     |\n" +
                        "|=====   |\n" +
                        "|==      |\n";
        assertEquals(expected, loader.load("index.html"));
    }

}