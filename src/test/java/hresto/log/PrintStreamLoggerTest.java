package hresto.log;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PrintStreamLoggerTest {

    Clock clock;
    ByteArrayOutputStream out, err;
    PrintStreamLogger logger;

    @Before
    public void setUp() throws Exception {
        out = new ByteArrayOutputStream();
        err = new ByteArrayOutputStream();
        clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        logger = new PrintStreamLogger(clock,
                new PrintStream(out), new PrintStream(err));
    }

    @Test
    public void log() {
        final List<Object> messages = asList(null, "", "This is a test.", 1234);
        messages.forEach(logger::log);
        final String expected = messages.stream()
                .map(msg -> clock.instant() + " " + String.valueOf(msg))
                .collect(joining("\n")) + "\n";
        assertEquals(expected, out.toString());
        assertEquals("", err.toString());
    }

    @Test
    public void error() {
        final List<String> messages = asList(null, "", "This is a test.");
        messages.forEach(msg -> logger.error(Severity.SEVERE, new RuntimeException(new NullPointerException()), msg));
        assertEquals("", out.toString());
        final String[] actualMessages = err.toString().split("\n[^\t]");
        for (String actual : actualMessages) {
            assertTrue(actual.contains(String.valueOf(clock.instant()).substring(1)));
            assertTrue(actual.contains(Severity.SEVERE.name()));
            assertTrue(actual.contains(RuntimeException.class.getSimpleName()));
            assertTrue(actual.contains(NullPointerException.class.getSimpleName()));
        }
    }

}